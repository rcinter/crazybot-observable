/* eslint-disable import/no-extraneous-dependencies */
const _ = require("lodash");
const path = require("path");
const TsConfigPathsPlugin = require("tsconfig-paths-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const nodeExternals = require("webpack-node-externals");
const TerserPlugin = require("terser-webpack-plugin");
const { ProgressPlugin } = require("webpack");
const TypescriptDeclarationPlugin = require("typescript-declaration-webpack-plugin");

const configBase = {
	watchOptions: {
		aggregateTimeout: 600,
		ignored: /node_modules/,
	},
	plugins: [
		new ProgressPlugin(),
		new CleanWebpackPlugin({
			cleanStaleWebpackAssets: false,
			cleanOnceBeforeBuildPatterns: [path.resolve(__dirname, "./dist")],
		}),
		new TypescriptDeclarationPlugin({ removeComments: false }),
	],
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				exclude: /node_modules/,
				use: [
					{
						loader: "ts-loader",
						options: {
							configFile: "tsconfig.build.json",
						}
					}
				]
			},
			{
				test: /\.css$/,
				use: [
					{
						loader: "style-loader"
					},
					{
						loader: "css-loader",
						options: {
							modules: true,
							sourceMap: true
						}
					}
				]
			}
		],
	},
	resolve: {
		extensions: [".tsx", ".ts", ".js"],
		plugins: [new TsConfigPathsPlugin({ configFile: "tsconfig.build.json" })],
		/*modules: [
			path.join(__dirname, "node_modules")
		]*/
	},
	optimization: {
		minimize: true,
		minimizer: [
			new TerserPlugin({
				test: /\.js(\?.*)?$/i,
				parallel: true
			})
		],
	}
}

const configBackend = {
	entry: {
		"backend": path.resolve(__dirname, "./packages/backend/src/index.ts"),
	},
	target: "node",
	externals: [nodeExternals(), "express"],
	output: {
		path: path.resolve(__dirname, "./dist/backend"),
		filename: "index.js",
		libraryTarget: "umd",
		library: "crazybot-observable/backend",
		umdNamedDefine: true
	}
};

const configComponents = {
	entry: {
		"components": path.resolve(__dirname, "./packages/components/src/index.ts"),
	},
	target: "web",
	externals: ["lodash", "react", "react-dom"],
	output: {
		path: path.resolve(__dirname, "./dist/components"),
		filename: "index.js",
		libraryTarget: "umd",
		library: "crazybot-observable/components",
		umdNamedDefine: true
	}
};

module.exports = (env, argv) => {
	console.log("Webpack env:", JSON.stringify(env, null, 2))
	switch (argv.mode) {
		case "development":
			//configBase.devtool = "cheap-module-source-map";
			configBase.devtool = "inline-source-map";
			break
		case "production":
			break
		default:
			throw new Error("Unknown mode: " + argv.mode);
	}

	return [
		configBackend,
		configComponents
	].map(projectConfig => _.merge(projectConfig, configBase));
};