# Crazybot Reactive Observable

This library is a Node.js implementation of the reactive observables concept, built on top of Express. It provides the ability to create observable resources with a subscription mechanism, and uses Server Sent Events (SSE) to notify listeners of updates.

## Project organization

This library is designed as a mono repository, based on [NX](https://nx.dev/). It consists of two packages: one for the backend side and one for the frontend.

The backend components are built on Node.js and Express, and are designed to handle observable resources using a subscription mechanism and Server Sent Events (SSE) to notify listeners of updates.

The frontend components are built using React and also use SSE to receive real-time updates from the server.

## Features

- Allows you to create two types of objects: "Observable services" and "Observable storages"
  
  - **Observable storage** is an object with ability to subscribe to it's updates. Implements 1 dimension in terms of listeners. All listeners will receive similar update events, when object underlying object changes
  - **Observable service** is 2 dimensional version of observable objects. Gives an ability for listeners to subscribe to same service but different "variants". "Service variant" is a `key` in **Observable service** structure.
  
- Uses [SSE](https://en.wikipedia.org/wiki/Server-sent_events) to notify listeners of updates in real-time
- Provides routing controllers for reading and writing to and from observable object
- Provides [React](https://react.dev/) reactive components for building user interfaces
- Implements [Proxy pattern](https://en.wikipedia.org/wiki/Proxy_pattern) to monitor target object changes
- Implements [Observer pattern](https://en.wikipedia.org/wiki/Observer_pattern)

## Installation

You can install the library using NPM:

```bash
npm install --save https://bitbucket.org/rcinter/crazybot-observable
```

## Usage

To use the library, you need to create an instance of either an "Observable service" or an "Observable storage" and then subscribe to updates.

### Creating an Observable Storage

To create an "Observable storage", you can use the following code:

```javascript
const storage = new ObservableStorage("storage-example", [])
setInterval(() => {
    const random = Math.round(Math.random() * 100);
    storage.observable.push(random)
}, 1000)

const storageMirror = {}
// Subscribe to observable updates
const unsubscribe = storage.subscribe(update => {
    // Merge changes
    console.log("update", update, "state", storage.observable);
})
```

### Creating an Observable Service

To create an "Observable service", you can use the following code:

```javascript
// First of all create a service that implements ServiceInterface
class ExampleService implements ServiceInterface {
    get(options: ExampleServiceVariantQuery): number[] {
        log("Fetching service initial data, variant", options);
        const initial = options.initial || 0;
        return [...Array(Number(initial)).keys()];
    }

    subscribe(updateCallback: ServiceUpdateCallback, options: ExampleServiceVariantQuery) {
        log("Opening service stream, variant", options);
        const timeout = options.timeout || 1000;

        // Create service connection
        const handler = setInterval(() => {
            const random = Math.round(Math.random() * 100);
            updateCallback({ random });
        }, Number(timeout));

        // Cleanup
        return () => {
            log("Closing service stream, variant", options);
            clearInterval(handler);
        };
    }
}

// Create example service instance
const srcService = new ExampleService()
// Create update handler
const callback: ObservableUpdateHandler = update => {
    // Merge changes
    console.log("updated", destination)
}

// Create name Observable that wraps a service
const service = new ObservableService("example", srcService)
const options: ServiceVariantQuery = { initial: 5, timeout: 1000 }
// Subscribe to service with options service
const unsubscribe = await service.subscribe(callback, options)
```
