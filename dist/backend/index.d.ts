export * from "./controller";
export * from "./core";
export * from "./services";
//# sourceMappingURL=index.d.ts.map
export * from "./express";
//# sourceMappingURL=index.d.ts.map
declare class BaseController<T extends ObservableServiceInterface> implements BaseControllerInterface {
    protected log: debug.Debugger;
    protected service: T;
    constructor(service: T);
    /**
     * Controller for obtaining service status
     * @param req express request object
     * @param res express response object
     * @returns
     */
    trace: (req: express.Request, res: express.Response, next: express.NextFunction) => Promise<void>;
}
export default BaseController;
//# sourceMappingURL=BaseController.d.ts.map
declare class ReadableController<T extends ObservableServiceInterface> extends BaseController<T> implements ReadableControllerInterface {
    protected log: debug.Debugger;
    constructor(service: T);
    /**
     * Controller for serving observable service
     * @param req express request object
     * @param res express response object
     * @returns
     */
    get: (req: express.Request, res: express.Response, next: express.NextFunction) => Promise<void>;
}
export default ReadableController;
//# sourceMappingURL=ReadableController.d.ts.map
declare class WritableController<T extends ObservableStorage> extends ReadableController<T> implements WritableControllerInterface {
    protected log: debug.Debugger;
    constructor(service: T);
    /**
     * Controller for updating service observable
     * @param req express request object
     * @param res express response object
     * @returns
     */
    set: (req: express.Request, res: express.Response, next: express.NextFunction) => void;
}
export default WritableController;
//# sourceMappingURL=WritableController.d.ts.map
export * from "./BaseController";
export * from "./ReadableController";
export * from "./types";
export * from "./WritableController";
export { BaseController, ReadableController, WritableController };
//# sourceMappingURL=index.d.ts.map
export interface BaseControllerInterface {
    trace: (req: express.Request, res: express.Response, next: express.NextFunction) => Promise<void> | void;
}
export interface ReadableControllerInterface {
    get: (req: express.Request, res: express.Response, next: express.NextFunction) => Promise<void> | void;
}
export interface WritableControllerInterface extends ReadableControllerInterface {
    set: (req: express.Request, res: express.Response, next: express.NextFunction) => Promise<void> | void;
}
//# sourceMappingURL=types.d.ts.map
/// <reference types="node" />
export declare enum ObservableOperations {
    UPDATE = "update"
}
declare class ObservableEventEmitter extends EventEmitter {
    /**
     * Emits observable "update" event
     * @param prop updated property key
     * @param value updated property value
     * @returns operation succeeded or not
     */
    emitUpdate(update: PropsUpdate): boolean;
    /**
     * Add new "update" event listener
     * @param listener listener to be executed
     * @returns cleanup function to be executed after listener removal
     */
    onUpdate(listener: ObservableUpdateHandler): () => this;
    /**
     * Return total listeners count
     * @returns
     */
    numListeners(): number;
}
export type ObservableEventEmitterOptions = {
    joinEvents?: boolean;
};
declare class ObservableEventEmitterBuffered extends ObservableEventEmitter {
    private options;
    private updatesBuffer;
    constructor(options?: ObservableEventEmitterOptions);
    /**
     * Emits observable "update" event
     * @param update updated key => value pairs
     * @returns operation succeeded or not
     */
    emitUpdate(update: PropsUpdate): boolean;
    /**
     * Emits observable "update" event with buffered updates
     * @param update updated key => value pairs
     * @returns always true
     */
    private emitUpdateBuffered;
}
export default ObservableEventEmitterBuffered;
//# sourceMappingURL=emitter.d.ts.map
export type CleanUpCallback = () => void;
export type PropKey = string | symbol | number;
export type PropValue = any;
export type PropsUpdate = {
    [key: PropKey]: PropValue;
};
export type ObservableUpdateHandler = (data: PropsUpdate) => void;
export type ObservableTarget = {
    [key: PropKey]: PropValue;
};
export type Observable = ObservableTarget & {
    onUpdate: (handler: ObservableUpdateHandler) => () => void;
    numListeners: () => number;
    numElements: (depth?: number) => number;
};
export type ObservableOptions = ObservableEventEmitterOptions & {
    recursive?: boolean;
};
export declare function isObservable(observableOrTarget?: Observable | ObservableTarget): observableOrTarget is Observable;
export declare function makeObservable(target?: ObservableTarget, options?: ObservableOptions): Observable;
export default makeObservable;
//# sourceMappingURL=index.d.ts.map
export declare class ObservableService implements ObservableServiceInterface {
    protected log: debug.IDebugger;
    name: string;
    /** Service for fetching data */
    private service;
    /** Service variants map (key: variant query params) */
    private serviceVariants;
    /** Observable object initialization options */
    private serviceVariantsOpts;
    /** Mutex for service variants */
    private mutex;
    /**
     * Calculates unique service variant id from service variant query
     * @param variantQuery service variant query
     * @returns unique service variant id
     */
    private static getVariantId;
    constructor(name: string, service: ServiceInterface, options?: ObservableOptions);
    /**
     * Retrieves existing or creates new service variant
     * @param variantQuery service variant query params
     * @returns service variant object
     */
    private getServiceVariant;
    /**
     * Subscribes to service variant updates
     * @param variantQuery service variant query params
     * @param userCallback realtime updates callback
     * @returns cleanup function
     */
    subscribe(userCallback: ObservableUpdateHandler, variantQuery?: ServiceVariantQuery): Promise<() => void>;
    /**
     * Get service variant observable object status
     * @param variantQuery service variant query params
     * @returns - service variant status
     */
    trace(variantQuery?: ServiceVariantQuery): Promise<import("./types").ObservableStatus | {
        [k: string]: import("./types").ObservableStatus;
    }>;
}
export default ObservableService;
//# sourceMappingURL=ObservableService.d.ts.map
declare class ObservableStorage implements ObservableServiceInterface {
    protected log: debug.IDebugger;
    name: string;
    observable: Observable;
    constructor(name: string, target?: ObservableTarget, options?: ObservableOptions);
    /**
     * Subscribes to observable updates
     * @param userCallback realtime updates callback
     * @returns cleanup function
     */
    subscribe(userCallback: ObservableUpdateHandler): () => void;
    /**
     * Get observable status
     */
    trace(): ObservableStatus;
}
export default ObservableStorage;
//# sourceMappingURL=ObservableStorage.d.ts.map
export * from "./ObservableService";
export * from "./ObservableStorage";
export * from "./types";
export { ObservableService, ObservableStorage };
//# sourceMappingURL=index.d.ts.map
/**
 * ServiceInterface -> ObservableService -> ObservableServiceInterface -> Controller adapter (express)
 */
/**
 * Service interface
 * Source interface for data fetching
 */
export interface ServiceInterface {
    /**
     * Initial data fetch from resource usinf query params
     * @param variantQuery fetch query params
     * @returns fetched data using query
     */
    get(variantQuery?: ServiceVariantQuery): Promise<ObservableTarget | undefined> | ObservableTarget | undefined;
    /**
     * Open new stream to service
     * @param variantQuery opens tream params
     * @param updateCallback stream update callback
     */
    subscribe(updateCallback: ServiceUpdateCallback, variantQuery?: ServiceVariantQuery): Promise<CleanUpCallback> | CleanUpCallback;
}
/**
 * Observable object status info
 */
export type ObservableStatus = {
    name: string;
    numListeners: number;
    numElements: number;
};
/**
 * Observable service observable object variants status map
 */
export type ObservableStatusMap = {
    [variantId: string]: ObservableStatus;
};
/**
 * Service controller interface
 * Destination interface for data pulling to controller
 */
export interface ObservableServiceInterface {
    name: string;
    subscribe(callback: ObservableUpdateHandler, variantQuery?: ServiceVariantQuery): Promise<CleanUpCallback> | CleanUpCallback;
    trace(variantQuery?: ServiceVariantQuery): Promise<ObservableStatus | ObservableStatusMap> | ObservableStatus | ObservableStatusMap;
}
/**
 * Service variant object
 */
export interface ServiceVariantType {
    storage: ObservableStorage;
    unsubscribe: CleanUpCallback;
}
/**
 * Service variant query params
 */
export type ServiceVariantQuery = {
    [key: string]: string | number;
};
/**
 * Service variant update callback
 */
export type ServiceUpdateCallback = (update: PropsUpdate) => void;
//# sourceMappingURL=types.d.ts.map
