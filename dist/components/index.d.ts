/// <reference types="react" />
export * from "./components";
export * from "./hooks";
export * from "./helpers";
export { components, hooks, helpers };
declare const _default: {
    useEventStream: typeof hooks.useEventStream;
    useResource: typeof hooks.useResource;
    useResourceTrace: typeof hooks.useResourceTrace;
    default: {
        useEventStream: typeof hooks.useEventStream;
        useResource: typeof hooks.useResource;
        useResourceTrace: typeof hooks.useResourceTrace;
    };
    ConnectionStatus: typeof hooks.ConnectionStatus;
    EventStream: import("react").FC<helpers.InferPropTypes<{
        url: import("prop-types").Validator<string>;
        children: import("prop-types").Requireable<NonNullable<((...args: any[]) => any) | import("prop-types").ReactNodeLike>>;
    }, {}, import("prop-types").InferProps<{
        url: import("prop-types").Validator<string>;
        children: import("prop-types").Requireable<NonNullable<((...args: any[]) => any) | import("prop-types").ReactNodeLike>>;
    }>>>;
    Resource: import("react").FC<helpers.InferPropTypes<{
        url: import("prop-types").Validator<string>;
        children: import("prop-types").Requireable<(...args: any[]) => any>;
    }, {
        children: null;
    }, import("prop-types").InferProps<{
        url: import("prop-types").Validator<string>;
        children: import("prop-types").Requireable<(...args: any[]) => any>;
    }>>>;
    ResourceTrace: import("react").FC<helpers.InferPropTypes<{
        url: import("prop-types").Validator<string>;
        children: import("prop-types").Requireable<(...args: any[]) => any>;
    }, {
        children: null;
    }, import("prop-types").InferProps<{
        url: import("prop-types").Validator<string>;
        children: import("prop-types").Requireable<(...args: any[]) => any>;
    }>>>;
};
export default _default;
//# sourceMappingURL=index.d.ts.map
declare const propTypes: {
    url: PropTypes.Validator<string>;
    children: PropTypes.Requireable<NonNullable<((...args: any[]) => any) | PropTypes.ReactNodeLike>>;
};
declare const defaultProps: {};
export type EventStreamOptions = InferPropTypes<typeof propTypes, typeof defaultProps>;
declare const EventStream: React.FC<EventStreamOptions>;
export default EventStream;
//# sourceMappingURL=EventStream.d.ts.map
declare const propTypes: {
    url: PropTypes.Validator<string>;
    children: PropTypes.Requireable<(...args: any[]) => any>;
};
declare const defaultProps: {
    children: null;
};
export type ResourceProps = InferPropTypes<typeof propTypes, typeof defaultProps>;
declare const Resource: React.FC<ResourceProps>;
export default Resource;
//# sourceMappingURL=Resource.d.ts.map
declare const propTypes: {
    url: PropTypes.Validator<string>;
    children: PropTypes.Requireable<(...args: any[]) => any>;
};
declare const defaultProps: {
    children: null;
};
export type ResourceTraceProps = InferPropTypes<typeof propTypes, typeof defaultProps>;
declare const ResourceTrace: React.FC<ResourceTraceProps>;
export default ResourceTrace;
//# sourceMappingURL=ResourceTrace.d.ts.map
export { EventStream, Resource, ResourceTrace };
declare const _default: {
    EventStream: import("react").FC<import("./EventStream").EventStreamOptions<{
        url: import("prop-types").Validator<string>;
        children: import("prop-types").Requireable<NonNullable<((...args: any[]) => any) | import("prop-types").ReactNodeLike>>;
    }, {}, import("prop-types").InferProps<{
        url: import("prop-types").Validator<string>;
        children: import("prop-types").Requireable<NonNullable<((...args: any[]) => any) | import("prop-types").ReactNodeLike>>;
    }>>>;
    Resource: import("react").FC<import("./EventStream").EventStreamOptions<{
        url: import("prop-types").Validator<string>;
        children: import("prop-types").Requireable<(...args: any[]) => any>;
    }, {
        children: null;
    }, import("prop-types").InferProps<{
        url: import("prop-types").Validator<string>;
        children: import("prop-types").Requireable<(...args: any[]) => any>;
    }>>>;
    ResourceTrace: import("react").FC<import("./EventStream").EventStreamOptions<{
        url: import("prop-types").Validator<string>;
        children: import("prop-types").Requireable<(...args: any[]) => any>;
    }, {
        children: null;
    }, import("prop-types").InferProps<{
        url: import("prop-types").Validator<string>;
        children: import("prop-types").Requireable<(...args: any[]) => any>;
    }>>>;
};
export default _default;
//# sourceMappingURL=index.d.ts.map
export type InferPropTypes<PropTypes, DefaultProps = Record<string, unknown>, Props = PropTypes.InferProps<PropTypes>> = {
    [Key in keyof Props]: Key extends keyof DefaultProps ? Props[Key] | DefaultProps[Key] : Props[Key];
};
//# sourceMappingURL=index.d.ts.map
export declare enum ConnectionStatus {
    CONNECTED = "Connected",
    CONNECTING = "Connecting",
    DISCONNECTED = "Disconnected",
    ERROR = "Error"
}
//# sourceMappingURL=enums.d.ts.map
export * from "./enums";
export { useEventStream, useResource, useResourceTrace };
declare const _default: {
    useEventStream: typeof useEventStream;
    useResource: typeof useResource;
    useResourceTrace: typeof useResourceTrace;
};
export default _default;
//# sourceMappingURL=index.d.ts.map
declare function useEventStream<T>(url: string): {
    event: T | undefined;
    status: ConnectionStatus;
    error: string | undefined;
};
export default useEventStream;
//# sourceMappingURL=useEventStream.d.ts.map
declare function useResource<T>(url: string, initialData: T): {
    data: T;
    event: T | undefined;
    status: import("./enums").ConnectionStatus;
    error: string | undefined;
};
export default useResource;
//# sourceMappingURL=useResource.d.ts.map
declare function useResourceTrace<T>(url: string): T | undefined;
export default useResourceTrace;
//# sourceMappingURL=useResourceTrace.d.ts.map
