// eslint-disable-next-line
import PropTypes from "prop-types";
export type InferPropTypes<
	PropTypes,
	DefaultProps = Record<string, unknown>,
	Props = PropTypes.InferProps<PropTypes>
	> = {
		[Key in keyof Props]: Key extends keyof DefaultProps
		? Props[Key] | DefaultProps[Key]
		: Props[Key]
	};