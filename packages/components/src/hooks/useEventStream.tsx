import { fetchEventSource } from "@microsoft/fetch-event-source";
import { useEffect, useState } from "react";
import { ConnectionStatus } from "./enums";

function useEventStream<T>(url: string) {
	const [event, setEvent] = useState<T>();
	const [status, setStatus] = useState<ConnectionStatus>(ConnectionStatus.DISCONNECTED)
	const [error, setError] = useState<string>()
	useEffect(() => {
		const ctrl = new AbortController();
		setStatus(ConnectionStatus.CONNECTING);
		fetchEventSource(url, {
			headers: {
				Accept: "text/event-stream",
			},
			onopen: async (res) => {
				if (res.ok && res.status === 200) {
					setStatus(ConnectionStatus.CONNECTED)
					setError(undefined);
				} else if (res.status >= 400 && res.status < 500 && res.status !== 429) {
					const error = await res.text();
					setStatus(ConnectionStatus.ERROR)
					setError([res.statusText, error].join(", "));
				}
			},
			onmessage: (event) => {
				try {
					const parsed: T = JSON.parse(event.data);
					setEvent(parsed);
				} catch (err) {
					setError("Error parsing event: " + event.data);
				}
			},
			onclose: () => {
				setStatus(ConnectionStatus.DISCONNECTED)
				setError(undefined)
			},
			onerror: (err) => {
				setStatus(ConnectionStatus.ERROR)
				setError("There was an error from server:" + err);
			},
			signal: ctrl.signal,
		});

		return () => ctrl.abort()
	}, [url]);

	return { event, status, error }
}

export default useEventStream