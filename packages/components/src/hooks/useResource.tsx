import _ from "lodash";
import { useEffect, useState } from "react";
import useEventStream from "./useEventStream";

function useResource<T>(url: string, initialData: T) {
	const { event, status, error } = useEventStream<T>(url)
	const [data, setData] = useState<T>(initialData);

	// Live data
	useEffect(() => {
		if (event)
			setData(data => _.merge(data, event));
	}, [event]);

	return { data, event, status, error }
}

export default useResource
