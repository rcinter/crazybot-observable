import { useEffect, useState } from "react";

function useResourceTrace<T>(url: string) {
	const [trace, setTrace] = useState<T>();
	const [counter, setCounter] = useState(0);

	useEffect(() => {
		const interval = setInterval(() => setCounter(counter => counter + 1), 1000);
		return () => clearInterval(interval);
	});

	// Live data
	useEffect(() => {
		if(!url) return;
		// Construct request headers
		const ctrl = new AbortController();
		const options = {
			headers: {
				"Accept": "application/json",
				"Content-Type": "application/json"
			},
			method: "GET",
			signal: ctrl.signal
		}
		// Executing RPC
		fetch(url + "/trace", options)
			.then(res => res.json())
			.then(data => {
				setTrace(data);
			})
		return () => ctrl.abort()
	}, [url, counter]);

	return trace
}

export default useResourceTrace
