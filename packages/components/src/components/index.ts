import EventStream from "./EventStream"
import Resource from "./Resource"
import ResourceTrace from "./ResourceTrace"

export { EventStream, Resource, ResourceTrace }
export default { EventStream, Resource, ResourceTrace }