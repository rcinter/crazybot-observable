import { ObservableStatus, ObservableStatusMap } from "@crazybot-observable/backend";
import PropTypes from "prop-types";
import React from "react";
import { InferPropTypes } from "../helpers";
import useResourceTrace from "../hooks/useResourceTrace";

const propTypes = {
	url: PropTypes.string.isRequired,
	children: PropTypes.func
}
const defaultProps = {
	children: null
}

export type ResourceTraceProps = InferPropTypes<typeof propTypes, typeof defaultProps>;

const ResourceTrace: React.FC<ResourceTraceProps> = ({ url, children }) => {
	const trace = useResourceTrace<ObservableStatus | ObservableStatusMap>(url);
	/*const [traces, setTraces] = useState<(ObservableStatus | ObservableStatusMap)[]>([]);

	// Save events
	useEffect(() => {
		if (trace)
			setTraces(traces => [...traces, trace]);
	}, [trace]);*/

	if (children && typeof children === "function")
		return children(trace);
	else
		return <div>
			<div>status: {trace && JSON.stringify(trace, null, 2)}</div>
			{/*<ol>{traces.map((event, index) => <li key={index}>{JSON.stringify(event)}</li>)}</ol>*/}
		</div>
}

ResourceTrace.propTypes = propTypes
ResourceTrace.defaultProps = defaultProps

export default ResourceTrace;
