import PropTypes from "prop-types";
import React from "react";
import { InferPropTypes } from "../helpers";
import useEventStream from "../hooks/useEventStream";

const propTypes = {
	url: PropTypes.string.isRequired,
	children: PropTypes.oneOfType([PropTypes.func, PropTypes.node])
}
const defaultProps = {}

export type EventStreamOptions = InferPropTypes<typeof propTypes, typeof defaultProps>;

const EventStream: React.FC<EventStreamOptions> = ({ url, children }) => {
	const { event, error, status } = useEventStream<object>(url);

	if (children && typeof children === "function")
		
		return children({ event, error, status })
	else return (
		<div>
			<div>status: {status} {error}</div>
			<div>event: {event && JSON.stringify(event, null, 2)}</div>
			{error && <div>error: {error}</div>}
		</div>
	);
};

EventStream.propTypes = propTypes
EventStream.defaultProps = defaultProps

export default EventStream
