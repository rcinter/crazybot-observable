import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { InferPropTypes } from "../helpers";
import useResource from "../hooks/useResource";

const propTypes = {
	url: PropTypes.string.isRequired,
	children: PropTypes.func
}
const defaultProps = {
	children: null
}

export type ResourceProps = InferPropTypes<typeof propTypes, typeof defaultProps>;

const Resource: React.FC<ResourceProps> = ({ url, children }) => {
	const { data, event, error, status } = useResource<object[]>(url, []);
	const [events, setEvents] = useState<object[]>([]);

	// Save events
	useEffect(() => {
		if (event)
			setEvents(events => [...events, event]);
	}, [event]);

	if (children && typeof children === "function")
		return children({ data, event, error, status });
	else
		return (
			<div>
				<div>status: {status} {error}</div>
				<hr />
				<div>last event: {event && JSON.stringify(event, null, 2)}</div>
				<hr />
				<div>
					data:
					<ul>{data.map((event, index) => <li key={index}>{JSON.stringify(event)}</li>)}</ul>
				</div>
				<hr />
				<div>
					events:
					<ol>{events.map((event, index) => <li key={index}>{JSON.stringify(event)}</li>)}</ol>
				</div>
			</div>
		);
}

Resource.propTypes = propTypes
Resource.defaultProps = defaultProps

export default Resource;
