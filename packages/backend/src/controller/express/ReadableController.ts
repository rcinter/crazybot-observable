import debug from "debug";
import express from "express";
import { ObservableUpdateHandler } from "../../core";
import { ObservableServiceInterface, ServiceVariantQuery } from "../../services";
import BaseController from "./BaseController";
import { ReadableControllerInterface } from "./types";

class ReadableController<T extends ObservableServiceInterface> extends BaseController<T> implements ReadableControllerInterface {
	protected log: debug.Debugger;

	constructor(service: T) {
		super(service);
		this.log = debug(`[controller:ro:${service.name}]`);
	}

	/**
	 * Controller for serving observable service
	 * @param req express request object
	 * @param res express response object
	 * @returns 
	 */
	get = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
		// Configure request for streaming
		req.socket.setTimeout(0);
		req.socket.setNoDelay(true);
		req.socket.setKeepAlive(true);
		req.statusCode = 200;

		// Configure response for streaming
		res.setHeader("Content-Type", "text/event-stream");
		res.setHeader("Cache-Control", "no-cache");
		res.setHeader("X-Accel-Buffering", "no");
		if (req.httpVersion !== "2.0")
			res.setHeader("Connection", "keep-alive");

		// flush the headers to establish SSE with client
		res.flushHeaders();

		// Prepare service subscription
		const query = Object.keys(req.query).length ? req.query : req.params;
		const onUpdate: ObservableUpdateHandler = (update) => {
			this.log("service update", update);
			// Send data over transport
			res.write(`data: ${JSON.stringify(update)}\n\n`);
			// Flush data (if compression is enabled)
			// eslint-disable-next-line @typescript-eslint/ban-ts-comment
			// @ts-ignore - TS doesn't know about res.flush
			if (res.flush) res.flush();
		};

		try {
			// Subscribe to service
			const unsubscribe = await this.service.subscribe(onUpdate, query as ServiceVariantQuery);

			// Abort controller
			let aborted = false;
			const onAbort = (err?: Error) => {
				if (aborted) return;
				this.log("service aborted", err?.message);
				aborted = true;
				// Close connection with service
				unsubscribe();
				// Close connection with client
				res.end(err);
			};

			// Bind to socket events
			res.on("close", onAbort);
			res.on("error", onAbort);
			res.on("finish", onAbort);

		} catch (err) {
			next(err);
		}
	};
}

export default ReadableController

/*eslint-disable no-console*/
/*async function main() {
	// Deploy server

	const app = express()
	const httpServer = http.createServer(app);
	app.use(express.json());
	app.use(cors());

	await new Promise((resolve: (value?: Record<string, unknown> | undefined) => void) => httpServer.listen(8080, resolve))

	const { ExampleService } = await import(path.resolve(__dirname, "../../examples/services"))
	// Create example observable service stream
	const service = new ObservableService("name", new ExampleService())
	const sse = new ReadableController(service)
	app.get('/service', sse.get);

	// Create example observable storage stream
	const storage = new ObservableStorage("example-storage", { a: 0, b: 0, c: [] })
	const sseStorage = new ReadableController(storage)
	app.get('/storage', sseStorage.get);

	setInterval(() => {
		storage.observable.a = Math.round(Math.random() * 100);
		storage.observable.b = Math.round(Math.random() * 100);
		storage.observable.c.push(Date.now())
	}, 1000)
}

const isFromCLI = process.argv[1] === __filename
if (isFromCLI) main()*/