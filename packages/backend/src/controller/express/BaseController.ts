import debug from "debug";
import express from "express";
import { ObservableServiceInterface, ServiceVariantQuery } from "../../services";
import { BaseControllerInterface } from "./types";

class BaseController<T extends ObservableServiceInterface> implements BaseControllerInterface {
	protected log: debug.Debugger;
	protected service: T;

	constructor(service: T) {
		this.log = debug(`[controller:status:${service.name}]`);
		this.service = service;
	}

	/**
	 * Controller for obtaining service status
	 * @param req express request object
	 * @param res express response object
	 * @returns 
	 */

	trace = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
		try {
			// Subscribe to service
			const query = Object.keys(req.query).length ? req.query : req.params;
			const status = await this.service.trace(query as ServiceVariantQuery);
			res.status(200);
			res.json(status);
		} catch (err) {
			next(err);
		}
	};
}

export default BaseController

/*eslint-disable no-console*/
/*async function main() {
	// Deploy server
	const app = express()
	const httpServer = http.createServer(app);

	app.use(express.json());
	app.use(cors());

	await new Promise((resolve: (value?: Record<string, unknown> | undefined) => void) => httpServer.listen(8080, resolve))

	const { ExampleService } = await import(path.resolve(__dirname, "../../examples/services"))
	// Create example observable service stream
	const service = new ObservableService("name", new ExampleService())

	// Subscribe to service
	service.subscribe(() => {
		// eslint-disable-next-line no-console
		console.log("service updated")
	}, { interval: 1000 })

	const sseService = new BaseController(service)
	app.get('/service/trace', sseService.trace);
	app.trace('/service', sseService.trace);

	// Create example observable storage stream
	const storage = new ObservableStorage("example-storage", { a: 0, b: 0, c: [] })
	const sseStorage = new BaseController(storage)
	app.get('/storage/trace', sseStorage.trace);
	app.trace('/storage', sseStorage.trace);

	setInterval(() => {
		storage.observable.a = Math.round(Math.random() * 100);
		storage.observable.b = Math.round(Math.random() * 100);
		storage.observable.c.push(Date.now())
	}, 1000)
}

const isFromCLI = process.argv[1] === __filename
if (isFromCLI) main()*/