import debug from "debug";
import express from "express";
import ObservableStorage from "../../services/ObservableStorage";
import ReadableController from "./ReadableController";
import { WritableControllerInterface } from "./types";

class WritableController<T extends ObservableStorage> extends ReadableController<T> implements WritableControllerInterface {
	protected log: debug.Debugger;
	constructor(service: T) {
		super(service);
		this.log = debug(`[controller:rw:${service.name}]`);
	}
	/**
	 * Controller for updating service observable
	 * @param req express request object
	 * @param res express response object
	 * @returns 
	 */
	set = (req: express.Request, res: express.Response, next: express.NextFunction) => {
		const updates = Object.keys(req.query).length ? req.query : req.params;
		try {
			// Apply updates to observable
			Object.entries(updates).forEach(([prop, value]) => {
				if (typeof value === "undefined")
					delete this.service.observable[prop];
				else
					this.service.observable[prop] = value;
			});

			res.status(200).end();
		} catch (err) {
			next(err);
		}
	};
}

export default WritableController

/*eslint-disable no-console*/
/*async function main() {
	// Deploy server
	const app = express()
	const httpServer = http.createServer(app);
	app.use(express.json());
	app.use(cors());

	await new Promise((resolve: (value?: Record<string, unknown> | undefined) => void) => httpServer.listen(8080, resolve))

	// Create example observable storage stream
	//const storage = new ObservableStorage("example-storage", { hello: "World" })
	//setInterval(() => {
	//	const random = Math.round(Math.random() * 100);
	//	storage.observable.push(random)
	//}, 1000)

	const sseStorage = new WritableController(storage)
	app.get("/storage", sseStorage.get);
	app.put("/storage", sseStorage.set);
}

const isFromCLI = process.argv[1] === __filename
if (isFromCLI) main()*/