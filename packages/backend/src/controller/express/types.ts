import express from "express";

export interface BaseControllerInterface {
	trace: (req: express.Request, res: express.Response, next: express.NextFunction) => Promise<void> | void;
}

export interface ReadableControllerInterface {
	get: (req: express.Request, res: express.Response, next: express.NextFunction) => Promise<void> | void;
}

export interface WritableControllerInterface extends ReadableControllerInterface {
	set: (req: express.Request, res: express.Response, next: express.NextFunction) => Promise<void> | void;
}
