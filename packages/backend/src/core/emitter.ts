import EventEmitter from "events";
import _ from "lodash";
import { ObservableUpdateHandler, PropsUpdate } from ".";
import debug from "debug";

const log = debug("observable:emitter");

export enum ObservableOperations {
	UPDATE = "update",
}

class ObservableEventEmitter extends EventEmitter {

	/**
	 * Emits observable "update" event
	 * @param prop updated property key
	 * @param value updated property value
	 * @returns operation succeeded or not
	 */
	emitUpdate(update: PropsUpdate): boolean {
		log("emitUpdate", update);
		return this.emit(ObservableOperations.UPDATE, update);
	}
	/**
	 * Add new "update" event listener
	 * @param listener listener to be executed
	 * @returns cleanup function to be executed after listener removal
	 */
	public onUpdate(listener: ObservableUpdateHandler) {
		this.on(ObservableOperations.UPDATE, listener);
		return () => this.off(ObservableOperations.UPDATE, listener);
	}
	/**
	 * Return total listeners count
	 * @returns
	 */
	numListeners() {
		return this.eventNames()
			.map(eventName => this.listenerCount(eventName))
			.reduce((acc, count) => acc + count, 0);
	}
}

export type ObservableEventEmitterOptions = {
	joinEvents?: boolean
}

// eslint-disable max-classes-per-file
class ObservableEventEmitterBuffered extends ObservableEventEmitter {
	private options: ObservableEventEmitterOptions | undefined;
	private updatesBuffer: PropsUpdate[] = []
	constructor(options?: ObservableEventEmitterOptions) {
		super()
		this.options = options
	}
	/**
	 * Emits observable "update" event
	 * @param update updated key => value pairs
	 * @returns operation succeeded or not
	 */
	emitUpdate(update: PropsUpdate): boolean {
		if (this.options?.joinEvents)
			return this.emitUpdateBuffered(update);
		return super.emitUpdate(update);
	}

	/**
	 * Emits observable "update" event with buffered updates
	 * @param update updated key => value pairs
	 * @returns always true
	 */
	private emitUpdateBuffered(update: PropsUpdate): boolean {
		// Combined message per channelName
		this.updatesBuffer = [...this.updatesBuffer || [], update]

		// Message per tick updates (separated by channel), bind tick callback only once
		if (this.updatesBuffer.length === 1) {
			process.nextTick(() => {
				let updates = update
				// Join updates
				if (this.updatesBuffer.length > 1)
					updates = _.merge({}, ...this.updatesBuffer)
				// Send joined updates
				super.emitUpdate(updates)
				// Clear buffer
				this.updatesBuffer.length = 0
			})
		}
		return true
	}
}

export default ObservableEventEmitterBuffered