import debug from "debug";
import _ from "lodash";
import { isProxy } from "util/types";
import ObservableEventEmitter, { ObservableEventEmitterOptions } from "./emitter";

const log = debug("observable:core");

export type CleanUpCallback = () => void
export type PropKey = string | symbol | number
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type PropValue = any
export type PropsUpdate = { [key: PropKey]: PropValue }
export type ObservableUpdateHandler = (data: PropsUpdate) => void
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type ObservableTarget = { [key: PropKey]: PropValue } // TODO: infer ObservableTarget type

export type Observable = ObservableTarget & {
	onUpdate: (handler: ObservableUpdateHandler) => () => void,
	numListeners: () => number,
	numElements: (depth?: number) => number
}

export type ObservableOptions = ObservableEventEmitterOptions & {
	recursive?: boolean
}

export function isObservable(observableOrTarget?: Observable | ObservableTarget): observableOrTarget is Observable {
	return typeof observableOrTarget !== "undefined"
		&& (observableOrTarget as Observable).onUpdate !== undefined
		&& (observableOrTarget as Observable).numListeners !== undefined
		&& (observableOrTarget as Observable).numElements !== undefined
		&& isProxy(observableOrTarget);
}

export function makeObservable(target: ObservableTarget = {}, options?: ObservableOptions): Observable {
	const recursive = (typeof options?.recursive !== "undefined") ? !!options.recursive : true
	const joinEvents = (typeof options?.joinEvents !== "undefined") ? !!options.joinEvents : true

	// Disable events joining on children props
	const childOptions = {
		recursive,
		joinEvents: false
	}

	// Create internal event emitter
	const emitter = new ObservableEventEmitter({ joinEvents })
	const listeners: Map<string, CleanUpCallback> = new Map()

	// Subscribe entry
	if (!target["onUpdate"])
		Object.defineProperty(target, "onUpdate", {
			value: (callback: ObservableUpdateHandler) => {
				const unsubscribe = emitter.onUpdate(callback)
				return () => {
					// Cleanup sub-listeners
					//Array.from(listeners.values()).forEach(close => close())
					unsubscribe()
				}
			},
			writable: false
		});

	// Number of observers
	if (!target["numListeners"])
		Object.defineProperty(target, "numListeners", {
			value: () => emitter.numListeners(),
			writable: false
		});

	// Number of elements
	if (!target["numElements"])
		Object.defineProperty(target, "numElements", {
			value: (depth?: number) => {
				if (typeof depth !== "undefined" && depth < 0) return 0
				let count = 0
				for (const key in target) {
					if (Object.prototype.hasOwnProperty.call(target, key)) {
						const value = target[key]
						if (isObservable(value))
							count += value.numElements(typeof depth !== "undefined" ? depth - 1 : undefined)
						else if (Array.isArray(value))
							count += value.length
						else if (typeof value === "object")
							count += Object.keys(value).length
						else
							count++
					}
				}
				return count
			},
			writable: false
		});

	// Target serialization
	if (!target["toJSON"])
		Object.defineProperty(target, "toJSON", {
			value: () => Array.isArray(target) ? [...target] : { ...target },
			writable: false
		});

	// Create target nested proxy objects
	if (recursive)
		for (const prop in target) {
			// Create new observable property if value is object
			if (Object.prototype.hasOwnProperty.call(target, prop) && target[prop] instanceof Object && !isObservable(target[prop])) {
				target[prop] = makeObservable(target[prop], childOptions)
				const callback: ObservableUpdateHandler = (update) => emitter.emitUpdate({ [prop]: update })
				const cleanup = target[prop].onUpdate(callback)
				listeners.set(String(prop), cleanup)
			}
		}

	// Let's create a proxy to react to changes
	return new Proxy(target, {
		set: ($target, prop, value, receiver) => {
			// Check for equality
			if (!_.isEqual(Reflect.get($target, prop, receiver), value)) {
				log(prop, "changed from", $target[prop], "to", value)
				// Create new observable property if value is object
				if (recursive)
					if (value instanceof Object && !isObservable(value)) {
						value = makeObservable(value, childOptions)
						const callback: ObservableUpdateHandler = (update) => emitter.emitUpdate({ [prop]: update })
						const cleanup = value.onUpdate(callback)
						listeners.set(String(prop), cleanup)
					}
				// Redirect operation to original object
				const success = Reflect.set($target, prop, value, receiver);
				// Call handlers on success
				if (success)
					emitter.emitUpdate({ [prop]: value })
				return success;
			}
			return true
		},
		deleteProperty: ($target, prop) => {
			log(prop, "deleted")
			if (prop in $target) {
				// Remove nested listeners
				const closeListener = listeners.get(String(prop))
				if (closeListener) closeListener()
				// Remove property itself
				delete $target[prop];
				// Emit event
				emitter.emitUpdate({ [prop]: undefined })
				return true
			} else return false
		}
	}) as Observable;
}

export default makeObservable;

/* eslint-disable no-console */
const isFromCLI = process.argv[1] === __filename
if (isFromCLI) {
	// Observable - object 
	/*{
		const target = Object.create({})
		// Source object
		const proxy = makeObservable(target, { joinEvents: true, recursive: true });
		// Destination object 
		const proxyMirror = Object.create({})

		// Subscribe to updates
		proxy.onUpdate(update => {
			// Merge changes
			_.merge(proxyMirror, update)
			console.log("updated", target, proxyMirror, proxy, proxy.numElements(0))
		});

		proxy.b = []

		setInterval(() => {
			proxy.a = { b: { c: Date.now() } }
			proxy.c = Date.now()
			proxy.b.push(Date.now())
			proxy.b.push({ random: Date.now() })
		}, 500)
	}*/
	// Observable - array
	{
		const target: number[] = []
		// Source object
		const proxy = makeObservable(target, { joinEvents: true, recursive: true });
		// Destination object 
		const proxyMirror: number[] = []
		// Subscribe to updates
		proxy.onUpdate(update => {
			// Merge changes
			_.merge(proxyMirror, update)
			console.log("updated", target, proxyMirror, proxy, proxy.numElements(0))
		});

		setInterval(() => {
			if (proxy.length >= 10)
				proxy.shift()
			console.log("push", proxy.length)
			proxy.push(Date.now())
			//proxy.push({ random: Date.now() })
		}, 1000)
	}
}