import debug from "debug";
import { Lock } from "semaphore-async-await";
import { ObservableOptions, ObservableUpdateHandler } from "../core";
import ObservableStorage from "./ObservableStorage";
import { ObservableServiceInterface, ServiceInterface, ServiceVariantQuery, ServiceVariantType } from "./types";

export class ObservableService implements ObservableServiceInterface {
	protected log: debug.IDebugger
	public name: string;

	/** Service for fetching data */
	private service: ServiceInterface;
	/** Service variants map (key: variant query params) */
	private serviceVariants: Map<string, ServiceVariantType> = new Map();
	/** Observable object initialization options */
	private serviceVariantsOpts: ObservableOptions;
	/** Mutex for service variants */
	private mutex: Lock = new Lock();

	/**
	 * Calculates unique service variant id from service variant query
	 * @param variantQuery service variant query
	 * @returns unique service variant id
	 */
	private static getVariantId(variantQuery?: ServiceVariantQuery) {
		if (variantQuery && Object.keys(variantQuery).length > 0)
			return Object.entries(variantQuery).map(entry => entry.join("=")).join("-");
		return "0"
	}

	constructor(name: string, service: ServiceInterface, options?: ObservableOptions) {
		this.log = debug(`[${name}]`);
		this.name = name
		this.service = service
		this.serviceVariantsOpts = {
			...options,
			// Create non-recursive ("plain") service variant observables
			recursive: false
		}
	}

	/**
	 * Retrieves existing or creates new service variant
	 * @param variantQuery service variant query params
	 * @returns service variant object
	 */
	private async getServiceVariant(variantQuery?: ServiceVariantQuery) {
		const variantId = ObservableService.getVariantId(variantQuery)
		await this.mutex.acquire();
		// Map variantQuery to variantId
		try {
			// Create variant if not exists
			if (!this.serviceVariants.has(variantId)) {
				this.log("Service starting, variant:", variantQuery);

				// Fetching initial data from service
				const initialData = await this.service.get(variantQuery);

				// Create observable proxy 
				const storage = new ObservableStorage(variantId, initialData, this.serviceVariantsOpts);

				// Open new service stream
				const closeServiceVariant = await this.service.subscribe((update) => {
					if (Array.isArray(storage.observable)) {
						storage.observable.push(update)
					} else {
						// Iterate over all observable props and apply update
						Object.entries(update).forEach(([key, value]) => {
							storage.observable[key] = value
						})
					}
				}, variantQuery);

				// cleanup subscription
				const unsubscribe = () => {
					this.log("Client disconnected, variant:", variantQuery);
					if (storage.observable.numListeners() === 0) {
						this.log("Service stopping, variant:", variantQuery);
						closeServiceVariant();
						this.serviceVariants.delete(variantId);
					} else {
						this.log(`Service serving ${storage.observable.numListeners()} listeners, variant:`, variantQuery);
					}
				}

				// Save newly created variant
				this.serviceVariants.set(variantId, { storage, unsubscribe });
				this.log("Service started, variant:", variantQuery);
			}

			// Return variant
			return this.serviceVariants.get(variantId);
		} finally {
			this.mutex.release()
		}
	}

	/**
	 * Subscribes to service variant updates
	 * @param variantQuery service variant query params
	 * @param userCallback realtime updates callback
	 * @returns cleanup function
	 */
	async subscribe(userCallback: ObservableUpdateHandler, variantQuery?: ServiceVariantQuery) {
		// Get service observable
		const { storage, unsubscribe } = await this.getServiceVariant(variantQuery) as ServiceVariantType;

		// Bind user callbacks to observable
		const closeListener = storage.subscribe((update) => userCallback(update));

		this.log("New client connected from service, variant:", variantQuery, ", listeners:", storage.observable.numListeners());
		return () => {
			// Close client listeners
			closeListener();
			// Unsubscribe from service
			process.nextTick(unsubscribe);
			this.log("Client disconnected from service, variant:", variantQuery, ", listeners:", storage.observable.numListeners());
		};
	}

	/**
	 * Get service variant observable object status 
	 * @param variantQuery service variant query params
	 * @returns - service variant status
	 */
	async trace(variantQuery?: ServiceVariantQuery) {
		if (variantQuery) {
			const variantId = ObservableService.getVariantId(variantQuery)
			if (!this.serviceVariants.has(variantId))
				throw new Error(`Service variant "${variantId}" not found`)
			else {
				const { storage } = await this.getServiceVariant(variantQuery) as ServiceVariantType;
				return storage.trace();
			}
		} else {
			return Object.fromEntries(
				Array.from(this.serviceVariants.entries()).map(([variantQuery, { storage }]) => {
					return [variantQuery, storage.trace()];
				})
			)
		}
	}
}

export default ObservableService

/*eslint-disable no-console*/
/*const isFromCLI = process.argv[1] === __filename
if (isFromCLI) {
	// Create service
	(async () => {
		const { ExampleService } = await import(path.resolve(__dirname, "../examples/services"))
		const srcService = new ExampleService()
		// Subscribe to service with options service
		const destination = {}
		const callback: ObservableUpdateHandler = update => {
			// Merge changes
			_.merge(destination, update)
			console.log("updated", destination)
		}
		const service = new ObservableService("example", srcService)
		const options: ServiceVariantQuery = { initial: 5, timeout: 1000 }
		const unsubscribe = await service.subscribe(callback, options)

		setTimeout(() => {
			unsubscribe()
		}, 5000)
	})()
}*/