import { CleanUpCallback, ObservableTarget, ObservableUpdateHandler, PropsUpdate } from "../core";
import ObservableStorage from "./ObservableStorage";

/**
 * ServiceInterface -> ObservableService -> ObservableServiceInterface -> Controller adapter (express)
 */

/**
 * Service interface
 * Source interface for data fetching
 */
export interface ServiceInterface {
	/**
	 * Initial data fetch from resource usinf query params
	 * @param variantQuery fetch query params
	 * @returns fetched data using query
	 */
	get(variantQuery?: ServiceVariantQuery): Promise<ObservableTarget | undefined> | ObservableTarget | undefined;
	/**
	 * Open new stream to service
	 * @param variantQuery opens tream params
	 * @param updateCallback stream update callback
	 */
	subscribe(updateCallback: ServiceUpdateCallback, variantQuery?: ServiceVariantQuery): Promise<CleanUpCallback> | CleanUpCallback;
}


/**
 * Observable object status info
 */
export type ObservableStatus = {
	name: string,
	numListeners: number,
	numElements: number
}
/**
 * Observable service observable object variants status map
 */
export type ObservableStatusMap = { [variantId: string]: ObservableStatus }

/**
 * Service controller interface
 * Destination interface for data pulling to controller
 */
export interface ObservableServiceInterface {
	name: string,
	subscribe(callback: ObservableUpdateHandler, variantQuery?: ServiceVariantQuery): Promise<CleanUpCallback> | CleanUpCallback;
	trace(variantQuery?: ServiceVariantQuery): Promise<ObservableStatus | ObservableStatusMap> | ObservableStatus | ObservableStatusMap;
}

/**
 * Service variant object
 */
export interface ServiceVariantType {
	storage: ObservableStorage,
	unsubscribe: CleanUpCallback
}

/**
 * Service variant query params
 */
export type ServiceVariantQuery = { [key: string]: string | number }

/**
 * Service variant update callback
 */
export type ServiceUpdateCallback = (update: PropsUpdate) => void