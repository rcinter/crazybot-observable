import debug from "debug";
import makeObservable, { Observable, ObservableOptions, ObservableTarget, ObservableUpdateHandler } from "../core";
import { ObservableServiceInterface, ObservableStatus } from "./types";

class ObservableStorage implements ObservableServiceInterface {
	protected log: debug.IDebugger;
	public name: string;
	public observable: Observable;

	constructor(name: string, target?: ObservableTarget, options?: ObservableOptions) {
		this.log = debug(`[${name}]`);
		this.name = name;
		this.observable = makeObservable(target, options);
	}

	/**
	 * Subscribes to observable updates
	 * @param userCallback realtime updates callback
	 * @returns cleanup function
	 */
	subscribe(userCallback: ObservableUpdateHandler) {
		// Bind user callbacks to observable
		const closeListener = this.observable.onUpdate((update) => userCallback(update));

		// Send initial observable value
		process.nextTick(() => userCallback(this.observable.toJSON()));

		this.log(`New client connected, listeners: ${this.observable.numListeners()}`);
		return () => {
			// Close client listeners
			closeListener();
			this.log(`Client disconnected, listeners: ${this.observable.numListeners()}`);
		};
	}

	/**
	 * Get observable status
	 */
	trace(): ObservableStatus {
		return {
			name: this.name,
			numListeners: this.observable.numListeners(),
			numElements: this.observable.numElements(),
		};
	}
}

export default ObservableStorage

/*eslint-disable no-console*/
/*const isFromCLI = process.argv[1] === __filename
if (isFromCLI) {
	// Wrap to observable storage
	const storage = new ObservableStorage("storage-example", [])
	setInterval(() => {
		const random = Math.round(Math.random() * 100);
		storage.observable.push(random)
	}, 1000)

	const storageMirror = {}
	// Subscribe to observable updates
	const unsubscribe = storage.subscribe(update => {
		// Merge changes
		_.merge(storageMirror, update);
		console.log("updated", storageMirror, storage.observable);
	})

	setTimeout(unsubscribe, 10000)
}*/