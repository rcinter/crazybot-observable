import { ObservableStorage } from "@crazybot-observable/backend";

class ExampleStorage extends ObservableStorage {
	constructor() {
		super("example-storage", []);
		setInterval(() => {
			const random = Math.round(Math.random() * 100);
			this.observable.push(random);
		}, 1000);
	}
}

export default ExampleStorage
