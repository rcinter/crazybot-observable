import { ServiceInterface, ServiceUpdateCallback, PropsUpdate, ObservableUpdateHandler } from "@crazybot-observable/backend";
import debug from "debug";


const log: debug.IDebugger = debug("ExampleService");

export type ExampleServiceVariantQuery = {
	timeout: number,
	initial: number
}

class ExampleService implements ServiceInterface {
	get(options: ExampleServiceVariantQuery): number[] {
		log("Fetching service initial data, variant", options);
		const initial = options.initial || 0;
		return [...Array(Number(initial)).keys()];
	}

	subscribe(updateCallback: ServiceUpdateCallback, options: ExampleServiceVariantQuery) {
		log("Opening service stream, variant", options);
		const timeout = options.timeout || 1000;

		// Create service connection
		const handler = setInterval(() => {
			const random = Math.round(Math.random() * 100);
			updateCallback({ random });
		}, Number(timeout));

		// Cleanup
		return () => {
			log("Closing service stream, variant", options);
			clearInterval(handler);
		};
	}
}

export default ExampleService

/*eslint-disable no-console*/
const isFromCLI = process.argv[1] === __filename
if (isFromCLI) {

	// Create service
	const srcService = new ExampleService()
	// Subscribe to service with options service
	const updates: PropsUpdate[] = []

	const callback: ObservableUpdateHandler = update => {
		// Merge changes
		updates.push(update)
		console.log("updated", updates)
	}

	const options: ExampleServiceVariantQuery = { initial: 5, timeout: 1000 }
	// Service warmup
	srcService.get(options)
	// Create service connection
	const unsubscribe = srcService.subscribe(callback, options)

	setTimeout(() => {
		unsubscribe()
	}, 5000)
}
