import ExampleService from "./service"
import ExampleStorage from "./storage"

export { ExampleService, ExampleStorage }