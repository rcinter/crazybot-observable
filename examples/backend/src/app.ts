//import "dotenv/config";
import compression from "compression";
import cors from "cors";
import express from "express";
import applyRoutes from "./routes";
const app = express();
const port = process.env.PORT || 3001;

export interface QueryPayload {
	payload: string;
}

// Down here is compression
app.use(compression({
	// filter decides if the response should be compressed or not, 
	// based on the `shouldCompress` function above
	filter: (req, res) => {
		if (req.headers["x-no-compression"]) {
			// don"t compress responses if this request header is present
			return false;
		}

		// fallback to standard compression
		return compression.filter(req, res);
	},
	// threshold is the byte threshold for the response body size
	// before compression is considered, the default is 1kb
	threshold: 0
}));

// CORS
app.use(cors({
	origin: '*'
}));
// here we are adding middleware to parse all incoming requests as JSON 
app.use(express.json());

// Apply all routes
applyRoutes(app);

app.get("/", (req, res) => {
	const responseData: QueryPayload = {
		payload: "Server data returned successfully",
	};
	res.json(responseData);
});

app.listen(port, () => {
	// eslint-disable-next-line no-console
	console.log(`Example app listening at http://localhost:${port}`);
});