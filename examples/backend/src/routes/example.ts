import { ObservableService, ReadableController, WritableController } from "@crazybot-observable/backend";
import express from "express";
import { ExampleService, ExampleStorage } from "../examples";
import BaseRoutes from "./common";

class ExampleRoutes extends BaseRoutes {
	constructor(app: express.Application) {
		super(app, "ExampleRoutes");
	}

	configureRoutes(): express.Application {

		// Serve example observable service stream
		const service = new ObservableService("example", new ExampleService())
		const controller = new ReadableController(service)
		this.app.get("/example/service", controller.get)
		this.app.get("/example/service/trace", controller.trace)

		// Serve example observable storage stream
		const sseStorage = new WritableController(new ExampleStorage())
		this.app.get("/example/storage", sseStorage.get);
		this.app.put("/example/storage", sseStorage.set);
		this.app.get("/example/storage/trace", sseStorage.trace);

		return this.app;
	}
}

export default ExampleRoutes