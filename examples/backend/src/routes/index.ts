import debug from "debug";
import { Application } from "express";
import CommonRoutes from "./common";
import ExampleRoutes from "./example";

const log: debug.IDebugger = debug("routes");

export default function applyRoutes(app: Application) {
	log("Applying routes ...")

	const routes: CommonRoutes[] = [];
	routes.push(new ExampleRoutes(app))

	log(`Applied routes: ${routes.length}`)
	return routes
}