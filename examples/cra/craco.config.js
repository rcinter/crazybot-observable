/* eslint-disable import/no-extraneous-dependencies */
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");
const ForkTSCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
const path = require("path");

module.exports = {
	eslint: { enable: false },
	webpack: {
		configure: (config) => {
			// Remove ModuleScopePlugin which throws when we try to import something
			// outside of src/.
			config.resolve.plugins.pop();

			// Resolve the path aliases.
			config.resolve.plugins.push(new TsconfigPathsPlugin());

			// Let Babel compile outside of src/.
			const oneOfRule = config.module.rules.find((rule) => rule.oneOf);
			const tsRule = oneOfRule.oneOf.find((rule) =>
				rule.test.toString().includes("ts|tsx")
			);

			tsRule.include = undefined;
			tsRule.exclude = /node_modules/;

			return config;
		},
		alias: {
			// "More than one copy of react in the same app" error fix
			//"react": path.resolve(__dirname, "node_modules/react"),
			"@crazybot-observable/components": path.resolve(__dirname, "node_modules/@crazybot-observable/components"),
			//"@crazybot-observable/backend": path.resolve(__dirname, "node_modules/@crazybot-observable/backend"),
		},
		plugins: {
			remove: [
				// This plugin is too old and causes problems in monorepos. We'll
				// replace it with a newer version.
				"ForkTsCheckerWebpackPlugin",
			],
			add: [
				// Use newer version of ForkTSCheckerWebpackPlugin to type check
				// files across the monorepo.
				new ForkTSCheckerWebpackPlugin({
					typescript: {
						configFile: "tsconfig.json",
					},
					issue: {
						// The exclude rules are copied from CRA.
						exclude: [
							{
								file: "**/src/**/__tests__/**",
							},
							{
								file: "**/src/**/?(*.)(spec|test).*",
							},
							{
								file: "**/src/setupProxy.*",
							},
							{
								file: "**/src/setupTests.*",
							},
						],
					},
				}),
			],
		},
	}
};
