import React from "react";
import "./App.css";

import { Resource, ResourceTrace } from "@crazybot-observable/components";

function App() {
	return <table style={{ width: "100%", tableLayout: "fixed" }}>
		<tbody>
			<tr>
				<td style={{ width: "100%", verticalAlign: "top" }}>
					<ResourceTrace url={"http://localhost:3001/example/service"} />
				</td>
				<td style={{ width: "100%", verticalAlign: "top" }}>
					<ResourceTrace url={"http://localhost:3001/example/storage"} />
				</td>
			</tr>
			<tr>
				<td style={{ width: "50%", verticalAlign: "top" }}>
					<a href="http://localhost:3001/example/service">/example/service</a>
					<Resource url={"http://localhost:3001/example/service"} />
				</td>
				<td style={{ width: "50%", verticalAlign: "top" }}>
					<a href="http://localhost:3001/example/storage">/example/storage</a>
					<Resource url={"http://localhost:3001/example/storage"} />
				</td>
			</tr>
		</tbody>
	</table>
}

export default React.memo(App);